## Cliente Web SGiS (Sistema de Gestión de Incidencias en Salud).

  
![SGiS](https://bitbucket.org/goraider/angular/src/4afc633b813b47daba17d5ece1dd82d8131ec1a9/src/assets/iconos/android-chrome-192x192.png)
  

### Descripción y contexto

  

Una de las prioridades de la Secretaría de Salud del Estado de Chiapas es tener herramientas para implementar acciones que permitan el acceso de las mujeres a los servicios obstétricos, a fin de reducir la muerte materna y neonatal; Por lo tanto, la creación de SGiS, es una estrategia para fortalecer la sistematización en atención de referencias, respuesta de urgencias, emergencias obstétricas y neonatales; cada unidad hospitalaria será la central de información; encargada de gestionar a pacientes en salud maternal, desde el monitoreo, registro y seguimiento de las incidencias; los procesos serán controlados a través de SGiS, estos para coordinar la red de servicios a fin de brindar una atención resolutiva a los usuarios dentro del menor tiempo posible.

  

Para contribuir a mejorar la calidad y eficacia de los servicios de salud deberá existir un sistema de referencia y respuesta que “constituya el enlace entre las unidades hospitalarias operativas de los niveles de atención que conforman la red de servicios, con el propósito de brindar a los usuarios atención médica integral y oportuna en las unidades, conforme al padecimiento de la paciente y la capacidad resolutiva de la unidad hospitalaria que resulten más convenientes”.

# UGUS-SGIS

- Angular 4
- Bulma.io
- Angular Cli

##### Software Requerido:

El desarrollo del Cliente web de SGiS se programó en [Angular 4](https://angular.io/)

  

Para poder instalar y utilizar el Cliente web, deberá asegurarse que su servidor cumpla con los siguientes requisitos, se dejan los links de descarga:

  

1. [Node.js](https://nodejs.org/es/) Descargar: "Recomendado para la mayoría"

2. [Npm](https://www.npmjs.com/get-npm) es un manejador de paquetes para el proyecto de Angular 4

3. [Git](https://git-scm.com/) es un sistema de control de versiones distribuidas de [código abierto y gratuito](https://git-scm.com/about/free-and-open-source).

4. [Angular CLI](https://cli.angular.io/) Hace que sea fácil crear una aplicación que ya funciona.

5. [Google Chrome](https://www.google.com.mx/intl/es_ALL/chrome/) El navegador optimo para acceder a SGiS.

6. [Visual Studio Code](https://code.visualstudio.com/download) El procesador de texto recomendado para codificar y explorar las carpetas de acuerdo a la estructura del proyecto SGiS.

*Si algo de lo anterior mencionado no se instalara correctamente, podrá consultar la documentación oficial de cada paquete de instalación*

## Instrucciones para publicar en producción

- Crear el directorio en el servidor donde se va alojar:

```
mkdir mi_proyecto  && cd mi_proyecto

Para Clonar el proyecto:
git clone https://goraider@bitbucket.org/goraider/angular.git


```
- Inicializar repositorio apuntando al proyecto, creando el repositorio remoto:
```
git init
git remote add origin https://goraider@bitbucket.org/goraider/angular.git
```

- Configuramos para que solo baje la carpeta del proyecto:
```
git pull origin master
```
- Dentro de la carpeta del Proyecto instalamos, para cargar todos los paquetes y sus dependencias:
```
npm install
```
- para compilar en produccion:

```
dentro de la carpeta del proyecto, este nos genera el proyecto compilado, ejecutamos:

ng build
```

- La estructura interna de nuestra carpeta quedaría como sigue
```
│ // La carpeta con nuestro build
├── tmp/
├──── dist/
│ // La carpeta app con los archvos html, js y css de nuestra aplicación 
│    ├── assets
│    ├── scripts
│    ├── favicon.ico
│    ├── index.html
.......etc.
```

- Configurar el archivo de apache **httpd.conf** para que apunte a la carpeta **tmp/dist/** del proyecto

```
<VirtualHost *:80>
    ServerAdmin john@doe.com
    DocumentRoot /var/www/html/mi_proyecto/dist
    ServerName mi.proyecto.com
    ErrorLog logs/error_log
    CustomLog logs/access_log combined
    <Directory /var/www/html/mi_proyecto/dist>
        RewriteEngine on

        # No reescribir archivos o directorios
        RewriteCond %{REQUEST_FILENAME} -f [OR]
        RewriteCond %{REQUEST_FILENAME} -d
        RewriteRule ^ - [L]

        # Reescribir todo lo demas a index.html para permitir html5 state links
        RewriteRule ^ index.html [L]
    </Directory>
</VirtualHost>
```

### Autor/es
---
* **[Javier Alejandro Gosain Díaz](https://github.com/goraider "Github")** - [Email](alejandro_gosain@hotmail.com)
* **[Ramiro Gabriel Alférez Chavez](mailto:ramiro.alferez@gmail.com "Correo electrónico")**
* **[Eliecer Ramirez Esquinca](https://github.com/checherman "Github")**
* **[Luis Alberto Valdez Lescieur](https://github.com/Luisvl13  "Github")**